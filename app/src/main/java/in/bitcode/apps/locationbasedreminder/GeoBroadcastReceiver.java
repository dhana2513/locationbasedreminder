package in.bitcode.apps.locationbasedreminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

import in.bitcode.apps.locationbasedreminder.utils.Util;


public class GeoBroadcastReceiver extends BroadcastReceiver {

    private Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;


        GeofencingEvent geofencingEvent =
                GeofencingEvent.fromIntent(intent);

        int transition = geofencingEvent.getGeofenceTransition();

        if ((transition == Geofence.GEOFENCE_TRANSITION_ENTER)) {

            List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();
            for (Geofence geofence : triggeringGeofences) {
                Util.getInstance(mContext).sendNotification(geofence.getRequestId());
            }
        } else {

            Log.e("tag", "invalidGeofence");
        }
    }
}



