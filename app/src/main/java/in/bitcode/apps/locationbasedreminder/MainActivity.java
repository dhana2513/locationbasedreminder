package in.bitcode.apps.locationbasedreminder;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import in.bitcode.apps.locationbasedreminder.fragments.FragmentReminderDetails;
import in.bitcode.apps.locationbasedreminder.pojo.Reminder;
import in.bitcode.apps.locationbasedreminder.utils.DBUtil;

import static in.bitcode.apps.locationbasedreminder.fragments.FragmentReminderList.KEY_REMINDER_DETAILS;
import static in.bitcode.apps.locationbasedreminder.utils.Util.KEY_EXTRA_REMINDER_ID;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Reminder mReminder;
    public static final String KEY_FLAG_NOTIFICATION = "KEY_FLAG_NOTIFICATION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        askPermission("android.permission.ACCESS_FINE_LOCATION", 1);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();

        if (intent.hasExtra(KEY_EXTRA_REMINDER_ID)) {

            int reminderId = intent.getExtras().getInt(KEY_EXTRA_REMINDER_ID, -1);


            Log.e("tag", reminderId + "");
            DBUtil.getInstance(this);

            String[] selectionArgs = new String[]{String.valueOf(reminderId)};
            Cursor c = DBUtil.mDbReminders.query("reminders", null, "reminder_id = ?", selectionArgs, null, null, null);

            while (c.moveToNext()) {
                String title = c.getString(1);
                String description = c.getString(2);
                LatLng location = new LatLng(Double.parseDouble(c.getString(3)),
                        Double.parseDouble(c.getString(4)));
                int radius = c.getInt(5);
                int repeat = c.getInt(6);
                int status = c.getInt(7);
                String date = c.getString(8);

                mReminder = new Reminder(reminderId, radius, title, description, location, repeat, status, date);
            }

            Bundle arguments = new Bundle();
            arguments.putSerializable(KEY_REMINDER_DETAILS, mReminder);
            int flagNotification = 1;
            arguments.putInt(KEY_FLAG_NOTIFICATION, flagNotification);
            FragmentReminderDetails fragmentReminderDetails = new FragmentReminderDetails();
            fragmentReminderDetails.setArguments(arguments);

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.mainContainer, fragmentReminderDetails)
                    .addToBackStack(null)
                    .commit();

        } else {

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }


    public void askPermission(String permission, int requestcode) {

        if (ContextCompat.checkSelfPermission(MainActivity.this, "android.permission.ACCESS_FINE_LOCATION"
        ) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permission}, requestcode);

            Toast.makeText(MainActivity.this, "Permission Granted", Toast.LENGTH_SHORT).show();
        } else {

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {


        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(MainActivity.this, "Permission granted",
                    Toast.LENGTH_SHORT).show();

        } else {
            Toast.makeText(MainActivity.this, "Permission is Denied",
                    Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.homeAsUp) {
        }

        if (id == R.id.home) {

        }

        if (id == R.id.disableHome) {

        }

        if (id == R.id.showHome) {

        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_disableAll) {

        } else if (id == R.id.nav_settings) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
