package in.bitcode.apps.locationbasedreminder.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;

import in.bitcode.apps.locationbasedreminder.R;
import in.bitcode.apps.locationbasedreminder.pojo.Reminder;
import in.bitcode.apps.locationbasedreminder.utils.DBUtil;


public class AdapterReminder extends RecyclerView.Adapter<AdapterReminder.ReminderHolder> {

    private ArrayList<Reminder> mListReminders;
    private OnReminderClickListener mOnReminderClickListener;
    private Context mContext;

    public AdapterReminder(ArrayList<Reminder> mListReminders) {
        this.mListReminders = mListReminders;
    }


    @Override
    public ReminderHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        mContext = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.lay_reminder, null);

        return new ReminderHolder(view);
    }

    @Override
    public void onBindViewHolder(ReminderHolder holder, int position) {

        String reminderName = mListReminders.get(position).getReminderName();
        String reminderDate = mListReminders.get(position).getReminderDate();

        if (mListReminders.get(position).getReminderStatus() == 1)
            holder.mSwitchStatus.setChecked(true);
        else
            holder.mSwitchStatus.setChecked(false);

        holder.mTxtReminderName.setText(reminderName);
        holder.mTxtReminderDescription.setText(reminderDate);

    }

    @Override
    public int getItemCount() {
        return mListReminders.size();
    }

    public class ReminderHolder extends RecyclerView.ViewHolder {

        private TextView mTxtReminderName;
        private TextView mTxtReminderDescription;
        private Switch mSwitchStatus;


        public ReminderHolder(View itemView) {
            super(itemView);

            mTxtReminderName = itemView.findViewById(R.id.txtReminderName);
            mTxtReminderDescription = itemView.findViewById(R.id.txtReminderDescription);
            mSwitchStatus = itemView.findViewById(R.id.switchStatus);

            mSwitchStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    DBUtil.getInstance(mContext).updateReminderStatus(
                            mListReminders.get(getAdapterPosition()).getReminderId(),
                            mSwitchStatus.isChecked() ? 1 : 0
                    );

                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mOnReminderClickListener.
                            onReminderClicked(mListReminders.get(getAdapterPosition()));
                }
            });
        }
    }

    public void setOnReminderClickListener(OnReminderClickListener onReminderClickListener) {

        mOnReminderClickListener = onReminderClickListener;
    }

    public interface OnReminderClickListener {

        void onReminderClicked(Reminder reminder);
    }
}
