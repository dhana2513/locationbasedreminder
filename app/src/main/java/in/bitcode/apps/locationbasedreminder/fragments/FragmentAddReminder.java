package in.bitcode.apps.locationbasedreminder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import in.bitcode.apps.locationbasedreminder.R;
import in.bitcode.apps.locationbasedreminder.utils.DBUtil;
import in.bitcode.apps.locationbasedreminder.utils.Util;

import static in.bitcode.apps.locationbasedreminder.fragments.FragmentReminderList.KEY_UPDATE_REMINDER_LIST;


public class FragmentAddReminder extends Fragment {

    private EditText mEdtTitle, mEdtDescription;
    private TextView mTxtLocation, mTxtSeekbarMinValue;
    private SeekBar mSeekBarRadius;
    private Switch mSwitchRepeat;
    private Button mBtnSet;
    private int mRadius;
    private FragmentReminderList.UpdateReminderList mUpdateReminderList;
    private Bundle mBundle;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_fragment_add_reminder, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Add Reminder");

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mBundle = getArguments();
        mUpdateReminderList = (FragmentReminderList.UpdateReminderList) mBundle.getSerializable(KEY_UPDATE_REMINDER_LIST);

        mEdtTitle = view.findViewById(R.id.edtTitle);
        mEdtDescription = view.findViewById(R.id.edtDescription);
        mTxtLocation = view.findViewById(R.id.txtLocation);
        mSeekBarRadius = view.findViewById(R.id.seekbar);
        mSwitchRepeat = view.findViewById(R.id.repeat);
        mBtnSet = view.findViewById(R.id.btnSet);
        mTxtSeekbarMinValue = view.findViewById(R.id.txtSeekBarMinValue);

        mTxtLocation.setText("LatLng: " + mBundle.getDouble("latitude") + " , " +
                mBundle.getDouble("longitude"));


        mSeekBarRadius.setProgress(1);
        mSeekBarRadius.setOnSeekBarChangeListener(new SeekBarListener());


        mBtnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());

                int reminderId =
                        DBUtil.getInstance(getContext()).addReminder(
                                mEdtTitle.getText().toString(),
                                mEdtDescription.getText().toString(),
                                "" + mBundle.getDouble("latitude"),
                                "" + mBundle.getDouble("longitude"),
                                mSeekBarRadius.getProgress() * 100,
                                mSwitchRepeat.isChecked() ? 1 : 0,
                                1,
                                currentDate
                        );

                Util.getInstance(getContext()).addReminderGeoFence(
                        reminderId,
                        mBundle.getDouble("latitude"),
                        mBundle.getDouble("longitude"),
                        mSeekBarRadius.getProgress() * 100
                );

                mUpdateReminderList.addLastAddedProduct(reminderId);

                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Reminders");

                getFragmentManager().popBackStack();
            }
        });

        return view;
    }


    private class SeekBarListener implements SeekBar.OnSeekBarChangeListener {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
            mRadius = progress;

            if (mRadius == 0) {
                seekBar.setProgress(1);
            }
            if (mRadius == 10) {
                mTxtSeekbarMinValue.setText("1Km");
            } else
                mTxtSeekbarMinValue.setText((mRadius * 100) + "m");
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    }
}
