package in.bitcode.apps.locationbasedreminder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import in.bitcode.apps.locationbasedreminder.R;
import in.bitcode.apps.locationbasedreminder.pojo.Reminder;
import in.bitcode.apps.locationbasedreminder.utils.DBUtil;
import in.bitcode.apps.locationbasedreminder.utils.Util;

import static in.bitcode.apps.locationbasedreminder.fragments.FragmentReminderList.KEY_REMINDER_DETAILS;
import static in.bitcode.apps.locationbasedreminder.fragments.FragmentReminderList.KEY_UPDATE_REMINDER_LIST;


public class FragmentEditReminder extends android.support.v4.app.Fragment {

    private EditText mEdtTitle, mEdtDescription;
    private SeekBar mSeekBarRadius;
    private TextView mTxtLocation, mTxtSeekbarMinValue;
    private Switch mSwitchRepeat;
    private Button mBtnSave;
    private Reminder mReminder;
    private int mRadius;
    private FragmentReminderList.UpdateReminderList mUpdateReminderList;
    private Bundle mBundle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_fragment_edit_reminder, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Edit Reminder");

        init(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mBundle = getArguments();

        mReminder = (Reminder) getArguments().getSerializable(KEY_REMINDER_DETAILS);
        mUpdateReminderList = (FragmentReminderList.UpdateReminderList) mBundle.getSerializable(KEY_UPDATE_REMINDER_LIST);

        mEdtTitle.setText(mReminder.getReminderName());
        mEdtDescription.setText(mReminder.getReminderDescription());
        mTxtLocation.setText(mBundle.getDouble("latitude") + " , " + mBundle.getDouble("longitude"));
        mRadius = mReminder.getReminderRadius();
        mSeekBarRadius.setProgress(mRadius / 100);
        mTxtSeekbarMinValue.setText(mRadius + "m");


        if (mReminder.getReminderRepeat() == 1)
            mSwitchRepeat.setChecked(true);
        else
            mSwitchRepeat.setChecked(false);

        mSeekBarRadius.setOnSeekBarChangeListener(new SeekBarListener());


        mBtnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mSeekBarRadius.getProgress() == 0) {
                    mSeekBarRadius.setProgress(1);
                }
                DBUtil.getInstance(getContext()).updateReminder(
                        mReminder.getReminderId(),
                        mEdtTitle.getText().toString(),
                        mEdtDescription.getText().toString(),
                        "" + mBundle.getDouble("latitude"),
                        "" + mBundle.getDouble("longitude"),
                        mSeekBarRadius.getProgress() * 100,
                        mSwitchRepeat.isChecked() ? 1 : 0,
                        mReminder.getReminderDate()
                );

                Util util = Util.getInstance(getContext());

                util.RemoveGeofence();
                util.addReminderGeoFence(
                        mReminder.getReminderId(),
                        mBundle.getDouble("latitude"),
                        mBundle.getDouble("longitude"),
                        mSeekBarRadius.getProgress() * 100
                );

                Log.e("tag", "Updated" + mReminder.getReminderId());

                mUpdateReminderList.updateList();

                ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Reminders");

                getFragmentManager().popBackStack();

            }
        });


        return view;
    }


    private void init(View view) {
        mEdtTitle = view.findViewById(R.id.edtTitle);
        mEdtDescription = view.findViewById(R.id.edtDescription);
        mTxtLocation = view.findViewById(R.id.txtLocation);
        mSeekBarRadius = view.findViewById(R.id.seekbar);
        mSwitchRepeat = view.findViewById(R.id.repeat);
        mTxtSeekbarMinValue = view.findViewById(R.id.txtSeekBarMinValue);
        mBtnSave = view.findViewById(R.id.btnSave);
    }

    private class SeekBarListener implements SeekBar.OnSeekBarChangeListener {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
            mRadius = progress;

            if (mRadius == 0) {
                seekBar.setProgress(1);
            }
            if (mRadius == 10) {
                mTxtSeekbarMinValue.setText("1Km");
            } else
                mTxtSeekbarMinValue.setText((mRadius * 100) + "m");
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }

}
