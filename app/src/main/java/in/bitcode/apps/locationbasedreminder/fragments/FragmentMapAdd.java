package in.bitcode.apps.locationbasedreminder.fragments;


import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import in.bitcode.apps.locationbasedreminder.R;


public class FragmentMapAdd extends Fragment implements OnMapReadyCallback {


    private SupportMapFragment mSupportMapFragment;
    private LocationManager mLocationManager;
    private GoogleMap mMap;
    private Marker mMarker = null;
    private Bundle mBundle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_fragment_map_add, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Choose Location");

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mBundle = getArguments();

        mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mSupportMapFragment = SupportMapFragment.newInstance();

        getFragmentManager().beginTransaction().
                replace(R.id.map, mSupportMapFragment).commit();

        mSupportMapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        init();
        mLocationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        CameraPosition cameraPosition = CameraPosition.builder().bearing(.5f)
                .target(new LatLng(20.5937, 78.9629))
                .tilt(30)
                .zoom(4)
                .build();

        CameraUpdate cameraUpdate =
                CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.moveCamera(cameraUpdate);

    }

    @SuppressLint("MissingPermission")
    private void init() {

        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMyLocationEnabled(true);
        mMap.setTrafficEnabled(true);
        mMap.setOnMapClickListener(new OnMapClick());
        mMap.setOnInfoWindowClickListener(new OnInfoWindowClick());

        UiSettings uiSettings = mMap.getUiSettings();

        uiSettings.setCompassEnabled(true);
        uiSettings.setIndoorLevelPickerEnabled(true);
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setRotateGesturesEnabled(true);
        uiSettings.setScrollGesturesEnabled(true);
        uiSettings.setTiltGesturesEnabled(true);
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);


    }

    private class OnMapClick implements GoogleMap.OnMapClickListener {

        @Override
        public void onMapClick(LatLng latLng) {

            if (mMarker == null) {
                mMarker = mMap.addMarker(
                        new MarkerOptions()
                                .position(latLng)
                                .title("Add Reminder")
                                .snippet("")
                );
                mMarker.setDraggable(true);
            } else {
                mMarker.setPosition(latLng);
            }

            CameraPosition.Builder builder = CameraPosition.builder();
            builder.bearing(.5f)
                    .target(mMarker.getPosition())
                    .tilt(30)
                    .zoom(20);
            CameraPosition cameraPosition = builder.build();

            CameraUpdate cameraUpdate =
                    CameraUpdateFactory.newCameraPosition(cameraPosition);

            mMap.animateCamera(cameraUpdate, 1000, null);
            mMarker.showInfoWindow();

        }
    }

    private class OnInfoWindowClick implements GoogleMap.OnInfoWindowClickListener {

        @Override
        public void onInfoWindowClick(Marker marker) {

            FragmentAddReminder fragmentAddReminder = new FragmentAddReminder();

            mBundle.putDouble("latitude", marker.getPosition().latitude);
            mBundle.putDouble("longitude", marker.getPosition().longitude);

            fragmentAddReminder.setArguments(mBundle);


            getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().addToBackStack(null).
                    add(R.id.mainContainer, fragmentAddReminder).commit();


        }
    }
}

