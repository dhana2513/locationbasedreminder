package in.bitcode.apps.locationbasedreminder.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import in.bitcode.apps.locationbasedreminder.R;
import in.bitcode.apps.locationbasedreminder.pojo.Reminder;

import static in.bitcode.apps.locationbasedreminder.fragments.FragmentReminderList.KEY_REMINDER_DETAILS;
import static in.bitcode.apps.locationbasedreminder.fragments.FragmentReminderList.KEY_UPDATE_REMINDER_LIST;


public class FragmentMapEdit extends Fragment implements OnMapReadyCallback {

    private SupportMapFragment mSupportMapFragment;
    private GoogleMap mMap;
    private Marker mMarker;
    private Reminder mReminder;
    private Bundle mBundle;
    private FragmentReminderList.UpdateReminderList mUpdateReminderList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_fragment_map_edit, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Choose Location");

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mBundle = getArguments();
        mReminder = (Reminder) getArguments().getSerializable(KEY_REMINDER_DETAILS);
        mUpdateReminderList = (FragmentReminderList.UpdateReminderList) mBundle.getSerializable(KEY_UPDATE_REMINDER_LIST);


        mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        mSupportMapFragment = SupportMapFragment.newInstance();

        getFragmentManager().beginTransaction().replace(R.id.map, mSupportMapFragment).commit();

        mSupportMapFragment.getMapAsync(this);


        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mMarker = mMap.addMarker(
                new MarkerOptions()
                        .position(mReminder.getReminderLocation())
                        .title("Edit Reminder")
                        .snippet("")
        );
        mMarker.setDraggable(true);
        mMarker.setPosition(mReminder.getReminderLocation());
        init();

        CameraPosition cameraPosition = CameraPosition.builder().bearing(.5f)
                .target(mMarker.getPosition())
                .tilt(30)
                .zoom(17)
                .build();

        CameraUpdate cameraUpdate =
                CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.moveCamera(cameraUpdate);
        mMarker.showInfoWindow();


    }

    @SuppressLint("MissingPermission")
    private void init() {

        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.setMyLocationEnabled(true);
        mMap.setTrafficEnabled(true);
        mMap.setOnMapClickListener(new OnMapClick());
        mMap.setOnInfoWindowClickListener(new OnInfoWindowClick());


        UiSettings uiSettings = mMap.getUiSettings();

        uiSettings.setCompassEnabled(true);
        uiSettings.setIndoorLevelPickerEnabled(true);
        uiSettings.setMapToolbarEnabled(true);
        uiSettings.setMyLocationButtonEnabled(true);
        uiSettings.setRotateGesturesEnabled(true);
        uiSettings.setScrollGesturesEnabled(true);
        uiSettings.setTiltGesturesEnabled(true);
        uiSettings.setZoomControlsEnabled(true);
        uiSettings.setZoomGesturesEnabled(true);


    }

    private class OnMapClick implements GoogleMap.OnMapClickListener {

        @Override
        public void onMapClick(LatLng latLng) {


            mMarker.setPosition(latLng);


            CameraPosition.Builder builder = CameraPosition.builder();
            builder.bearing(.5f)
                    .target(mMarker.getPosition())
                    .tilt(90)
                    .zoom(15);
            CameraPosition cameraPosition = builder.build();

            CameraUpdate cameraUpdate =
                    CameraUpdateFactory.newCameraPosition(cameraPosition);

            mMap.animateCamera(cameraUpdate, 1000, null);


            mMarker.showInfoWindow();

        }
    }

    private class OnInfoWindowClick implements GoogleMap.OnInfoWindowClickListener {

        @Override
        public void onInfoWindowClick(Marker marker) {
            FragmentEditReminder fragmentEditReminder = new FragmentEditReminder();

            // Code to do Some Task
            mBundle.putDouble("latitude", marker.getPosition().latitude);
            mBundle.putDouble("longitude", marker.getPosition().longitude);

            fragmentEditReminder.setArguments(mBundle);
            getFragmentManager().popBackStack();
            getFragmentManager().beginTransaction().addToBackStack(null).
                    add(R.id.mainContainer, fragmentEditReminder).commit();
        }
    }


}
