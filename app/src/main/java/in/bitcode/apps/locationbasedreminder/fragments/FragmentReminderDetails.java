package in.bitcode.apps.locationbasedreminder.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import in.bitcode.apps.locationbasedreminder.R;
import in.bitcode.apps.locationbasedreminder.pojo.Reminder;
import in.bitcode.apps.locationbasedreminder.utils.DBUtil;
import in.bitcode.apps.locationbasedreminder.utils.Util;

import static in.bitcode.apps.locationbasedreminder.MainActivity.KEY_FLAG_NOTIFICATION;
import static in.bitcode.apps.locationbasedreminder.fragments.FragmentReminderList.KEY_REMINDER_DETAILS;
import static in.bitcode.apps.locationbasedreminder.fragments.FragmentReminderList.KEY_UPDATE_REMINDER_LIST;


public class FragmentReminderDetails extends Fragment {

    private TextView mTxtTitle, mTxtDescription, mTxtLocation, mTxtMinValueRadius;
    private Switch mSwitchRepeat;
    private int mRadius;
    private Reminder mReminder;
    private FloatingActionButton mFloatingBtnEdit, mFloatingBtnDelete;
    private AlertDialog.Builder mBuilder;
    private FragmentReminderList.UpdateReminderList mUpdateReminderList;
    private Bundle mBundle;
    private GoogleMap mMap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_fragment_reminder_details, null);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Reminder Details");

        init(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mSwitchRepeat.setClickable(false);

        mBundle = getArguments();
        mReminder = (Reminder) mBundle.getSerializable(KEY_REMINDER_DETAILS);
        mUpdateReminderList = (FragmentReminderList.UpdateReminderList) mBundle.getSerializable(KEY_UPDATE_REMINDER_LIST);

        mTxtTitle.setText(mReminder.getReminderName());
        mTxtDescription.setText(mReminder.getReminderDescription());
        mTxtLocation.setText(mReminder.getReminderLocation() + "");
        mRadius = mReminder.getReminderRadius();
        mTxtMinValueRadius.setText(mRadius + "m");

        try {
            if (mBundle.getInt(KEY_FLAG_NOTIFICATION) == 1) {
                mFloatingBtnDelete.setVisibility(View.INVISIBLE);
                mFloatingBtnEdit.setVisibility(View.INVISIBLE);
            }

        } catch (Exception e) {
        }

        int isRepeat = mReminder.getReminderRepeat();
        if (isRepeat == 1) {
            mSwitchRepeat.setChecked(true);
        } else {
            mSwitchRepeat.setChecked(false);
        }


        mFloatingBtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                FragmentMapEdit fragmentMapEdit = new FragmentMapEdit();

                fragmentMapEdit.setArguments(mBundle);

                getFragmentManager().popBackStack();
                fragmentTransaction.add(R.id.mainContainer, fragmentMapEdit);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            }
        });

        mFloatingBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mBuilder = new AlertDialog.Builder(getContext());

                mBuilder.setTitle("Delete");
                mBuilder.setMessage("Are you sure, you want to delete this reminder?");
                mBuilder.setIcon(R.drawable.ic_delete_black_24dp);

                AlertDialogClickListener listener = new AlertDialogClickListener();

                mBuilder.setPositiveButton("Yes", listener);
                mBuilder.setNegativeButton("Cancel", listener);
                mBuilder.setCancelable(false);
                mBuilder.create().show();
                Util.getInstance(getContext()).RemoveGeofence();

            }
        });


        return view;
    }

    private class AlertDialogClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialogInterface, int which) {
            if (which == DialogInterface.BUTTON_POSITIVE) {

                DBUtil.getInstance(getActivity()).deleteReminder(mReminder.getReminderId());

                mUpdateReminderList.updateList();

                getFragmentManager().popBackStack();

                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);

            } else if (which == DialogInterface.BUTTON_NEGATIVE) {


                mBuilder.create().dismiss();

            }

        }
    }


    private void init(View view) {

        mTxtTitle = view.findViewById(R.id.txtTitle);
        mTxtDescription = view.findViewById(R.id.txtDescription);
        mTxtLocation = view.findViewById(R.id.txtLocation);
        mTxtMinValueRadius = view.findViewById(R.id.txtSeekBarMinValue);
        mFloatingBtnEdit = view.findViewById(R.id.fabEdit);
        mFloatingBtnDelete = view.findViewById(R.id.fabDelete);
        mSwitchRepeat = view.findViewById(R.id.repeat);

        SupportMapFragment mapFragment;
        mapFragment = SupportMapFragment.newInstance();

        getFragmentManager().beginTransaction().
                replace(R.id.details_map, mapFragment).commit();

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                mMap = googleMap;
                Marker marker = mMap.addMarker(new MarkerOptions().position(mReminder.getReminderLocation()));

                CameraPosition cameraPosition = CameraPosition.builder().bearing(.5f)
                        .target(marker.getPosition())
                        .tilt(30)
                        .zoom(mReminder.getReminderRadius() > 500 ? 13 : 14)
                        .build();

                CameraUpdate cameraUpdate =
                        CameraUpdateFactory.newCameraPosition(cameraPosition);
                mMap.moveCamera(cameraUpdate);

                mMap.addCircle(new CircleOptions().center(marker.getPosition()).radius(mReminder.getReminderRadius()).
                        fillColor(0x220000FF).strokeColor(Color.BLUE).strokeWidth(1));

            }
        });

    }
}
