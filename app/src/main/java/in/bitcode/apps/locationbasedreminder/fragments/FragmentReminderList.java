package in.bitcode.apps.locationbasedreminder.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;

import in.bitcode.apps.locationbasedreminder.R;
import in.bitcode.apps.locationbasedreminder.adapters.AdapterReminder;
import in.bitcode.apps.locationbasedreminder.pojo.Reminder;
import in.bitcode.apps.locationbasedreminder.utils.DBUtil;


public class FragmentReminderList extends Fragment {

    private RecyclerView mRecyclerView;
    private ArrayList<Reminder> mListReminders;
    private AdapterReminder mAdapterReminder;
    private Reminder mReminder;
    private FloatingActionButton mFloatingBtnAdd;

    public static final String KEY_UPDATE_REMINDER_LIST = "updateReminderList";
    public static final String KEY_REMINDER_DETAILS = "reminderDetails";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.lay_reminder_list, null);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        mRecyclerView = view.findViewById(R.id.recyclerView);
        mFloatingBtnAdd = view.findViewById(R.id.btnFloatingAdd);


        mFloatingBtnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentMapAdd fragmentMapAdd = new FragmentMapAdd();

                Bundle bundle = new Bundle();
                bundle.putSerializable(KEY_UPDATE_REMINDER_LIST, new UpdateReminderList());

                fragmentMapAdd.setArguments(bundle);

                getFragmentManager().beginTransaction().addToBackStack(null).
                        add(R.id.mainContainer, fragmentMapAdd).commit();
            }
        });

        mListReminders = DBUtil.getInstance(getContext()).getReminders();

        mAdapterReminder = new AdapterReminder(mListReminders);

        mRecyclerView.setAdapter(mAdapterReminder);


        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        mAdapterReminder.setOnReminderClickListener(new MyOnReminderClickListener());

        return view;


    }


    public class MyOnReminderClickListener implements AdapterReminder.OnReminderClickListener {
        @Override
        public void onReminderClicked(Reminder reminder) {

            mReminder = reminder;

            FragmentReminderDetails fragmentReminderDetails =
                    new FragmentReminderDetails();

            Bundle bundle = new Bundle();

            bundle.putSerializable(KEY_REMINDER_DETAILS, mReminder);
            bundle.putSerializable(KEY_UPDATE_REMINDER_LIST, new UpdateReminderList());

            fragmentReminderDetails.setArguments(bundle);

            getFragmentManager().beginTransaction().addToBackStack(null).
                    add(R.id.mainContainer, fragmentReminderDetails).commit();


        }


    }


    public class UpdateReminderList implements Serializable {

        public void updateList() {

            mListReminders.clear();
            mListReminders.addAll(DBUtil.getInstance(getContext()).getReminders());
            mAdapterReminder.notifyDataSetChanged();

        }

        public void addLastAddedProduct(int reminderId) {

            Reminder reminder = DBUtil.getInstance(getContext()).getLastAddedProduct(reminderId);
            mListReminders.add(reminder);
            mAdapterReminder.notifyDataSetChanged();
        }
    }


}
