package in.bitcode.apps.locationbasedreminder.pojo;

import com.google.android.gms.maps.model.LatLng;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;


public class Reminder implements Externalizable {

    private int mReminderId, mReminderRadius;
    private String mReminderName, mReminderDescription;
    private LatLng mReminderLocation;
    private int mReminderRepeat, mReminderStatus;
    private String mReminderDate;

    public Reminder() {
    }

    public Reminder(int reminderId, int reminderRadius, String reminderName, String reminderDescription, LatLng reminderLocation, int reminderRepeat, int reminderStatus, String reminderDate) {
        mReminderId = reminderId;
        mReminderRadius = reminderRadius;
        mReminderName = reminderName;
        mReminderDescription = reminderDescription;
        mReminderLocation = reminderLocation;
        mReminderRepeat = reminderRepeat;
        mReminderStatus = reminderStatus;
        mReminderDate = reminderDate;
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {


        out.writeDouble(mReminderLocation.latitude);
        out.writeDouble(mReminderLocation.longitude);


    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        mReminderLocation = new LatLng(in.readDouble(), in.readDouble());
    }

    public int getReminderId() {
        return mReminderId;
    }

    public void setReminderId(int reminderId) {
        mReminderId = reminderId;
    }

    public int getReminderRadius() {
        return mReminderRadius;
    }

    public void setReminderRadius(int reminderRadius) {
        mReminderRadius = reminderRadius;
    }

    public String getReminderName() {
        return mReminderName;
    }

    public void setReminderName(String reminderName) {
        mReminderName = reminderName;
    }

    public String getReminderDescription() {
        return mReminderDescription;
    }

    public void setReminderDescription(String reminderDescription) {
        mReminderDescription = reminderDescription;
    }

    public LatLng getReminderLocation() {
        return mReminderLocation;
    }

    public void setReminderLocation(LatLng reminderLocation) {
        mReminderLocation = reminderLocation;
    }

    public int getReminderRepeat() {
        return mReminderRepeat;
    }

    public void setReminderRepeat(int reminderRepeat) {
        mReminderRepeat = reminderRepeat;
    }

    public int getReminderStatus() {
        return mReminderStatus;
    }

    public void setReminderStatus(int reminderStatus) {
        mReminderStatus = reminderStatus;
    }

    public String getReminderDate() {
        return mReminderDate;
    }

    public void setReminderDate(String reminderDate) {
        this.mReminderDate = reminderDate;
    }
}