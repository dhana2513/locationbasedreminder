package in.bitcode.apps.locationbasedreminder.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import in.bitcode.apps.locationbasedreminder.pojo.Reminder;


public class DBUtil {

    public static final String DB_NAME = "DB_REMINDER";
    public static SQLiteDatabase mDbReminders = null;
    private static DBUtil dbUtil;

    private DBUtil(Context context) {

        mDbReminders = new ReminderHelper(context, DB_NAME, null, 1)
                .getWritableDatabase();
    }

    public static DBUtil getInstance(Context context) {

        if (dbUtil == null) {
            dbUtil = new DBUtil(context);
        }
        return dbUtil;
    }

    public int addReminder(String reminderName, String reminderDescription, String locationLat, String locationLng, int reminderRadius, int reminderRepeat, int reminderStatus, String reminderDate) {

        int reminderId = 1;

        Cursor c = mDbReminders.rawQuery("select Max(reminder_id) from reminders", null);

        if (c.moveToNext())
            reminderId = c.getInt(0) + 1;

        c.close();

        ContentValues values = new ContentValues();

        values.put("reminder_id", reminderId);
        values.put("reminder_name", reminderName);
        values.put("reminder_description", reminderDescription);
        values.put("reminder_lat", locationLat);
        values.put("reminder_lng", locationLng);
        values.put("reminder_radius", reminderRadius);
        values.put("reminder_repeat", reminderRepeat);
        values.put("reminder_status", reminderStatus);
        values.put("reminder_date", reminderDate);

        Log.e("tag", "Added: " + " " + reminderId + " " + reminderName + " " + reminderDescription + " " + locationLat + " " + locationLng + " " + " " + reminderRadius + " " + reminderRepeat + " " + reminderStatus + " " + reminderDate);

        mDbReminders.insert("reminders", null, values);

        return reminderId;
    }

    public boolean updateReminder(int reminderId, String reminderName, String reminderDescription, String locationLat, String locationLng, int reminderRadius, int reminderRepeat, String reminderDate) {

        ContentValues values = new ContentValues();
        values.put("reminder_id", reminderId);
        values.put("reminder_name", reminderName);
        values.put("reminder_description", reminderDescription);
        values.put("reminder_lat", locationLat);
        values.put("reminder_lng", locationLng);
        values.put("reminder_radius", reminderRadius);
        values.put("reminder_repeat", reminderRepeat);
        values.put("reminder_date", reminderDate);

        Log.e("tag", "Updated: " + reminderId + " " + reminderName + " " + reminderDescription + " " + locationLat + " " + locationLng + " " + reminderRadius + " " + reminderRepeat);

        String[] selectionArgs = new String[]{String.valueOf(reminderId)};

        int count =
                mDbReminders.update("reminders",
                        values,
                        "reminder_id = ?",
                        selectionArgs
                );

        if (count >= 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteReminder(int reminderId) {

        String[] selectionArgs = new String[]{String.valueOf(reminderId)};

        int count = mDbReminders.delete("reminders",
                "reminder_id = ?",
                selectionArgs);

        Log.e("tag", " deleted " + reminderId);

        if (count >= 0) {
            return true;
        } else {
            return false;
        }

    }


    public ArrayList<Reminder> getReminders() {

        ArrayList<Reminder> listReminders = new ArrayList<>();

        Cursor cursor = mDbReminders.query("reminders", null, null, null, null, null, null);


        while (cursor.moveToNext()) {

            Reminder reminder = new Reminder();
            reminder.setReminderId(cursor.getInt(0));
            reminder.setReminderName(cursor.getString(1));
            reminder.setReminderDescription(cursor.getString(2));
            reminder.setReminderLocation(
                    new LatLng(Double.parseDouble(cursor.getString(3)),
                            Double.parseDouble(cursor.getString(4))));

            reminder.setReminderRadius(cursor.getInt(5));
            reminder.setReminderRepeat(cursor.getInt(6));
            reminder.setReminderStatus(cursor.getInt(7));
            reminder.setReminderDate(cursor.getString(8));

            listReminders.add(reminder);
        }

        cursor.close();

        return listReminders;

    }

    public void updateReminderStatus(int reminderId, int reminderStatus) {

        String[] selectionArgs = new String[]{String.valueOf(reminderId)};

        ContentValues values = new ContentValues();
        values.put("reminder_status", reminderStatus);
        int cnt = mDbReminders.update("reminders", values, "reminder_id=?", selectionArgs);
        Log.e("tag", "Status Updated id : " + reminderId + "  Status: " + reminderStatus);
    }

    public Reminder getLastAddedProduct(int reminderId) {

        String[] selectionArgs = new String[]{String.valueOf(reminderId)};
        Cursor cursor = mDbReminders.query("reminders", null, "reminder_id=?", selectionArgs,
                null, null, null);

        Reminder reminder = new Reminder();

        if (cursor.moveToNext()) {

            reminder.setReminderId(cursor.getInt(0));
            reminder.setReminderName(cursor.getString(1));
            reminder.setReminderDescription(cursor.getString(2));

            reminder.setReminderLocation(
                    new LatLng(Double.parseDouble(cursor.getString(3)),
                            Double.parseDouble(cursor.getString(4)))
            );
            reminder.setReminderRadius(cursor.getInt(5));
            reminder.setReminderRepeat(cursor.getInt(6));
            reminder.setReminderStatus(cursor.getInt(7));
            reminder.setReminderDate(cursor.getString(8));
        }

        return reminder;
    }

    public static class ReminderHelper extends SQLiteOpenHelper {

        public ReminderHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {

            sqLiteDatabase.execSQL("create table reminders(reminder_id Integer primary key, reminder_name text, reminder_description text, reminder_lat text , reminder_lng text, reminder_radius Integer, reminder_repeat Integer, reminder_status Integer,reminder_date text)");


        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            super.onDowngrade(db, oldVersion, newVersion);
        }
    }
}

