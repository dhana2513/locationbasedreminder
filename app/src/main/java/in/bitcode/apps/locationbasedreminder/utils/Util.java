package in.bitcode.apps.locationbasedreminder.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

import in.bitcode.apps.locationbasedreminder.MainActivity;
import in.bitcode.apps.locationbasedreminder.R;
import in.bitcode.apps.locationbasedreminder.pojo.Reminder;

public class Util {

    public static final String ACTION_ENTER = "in.bitcode.apps.locationbasedreminder.ENTER";
    public static final String KEY_EXTRA_REMINDER_ID = "reminderid";
    public static final String KEY_REMINDER_OBJ = "reminderObj";
    private GeofencingClient mGeofencingClient;
    private NotificationManager mNotificationManager;
    private int mReminderId, PENDING_INTENT_ID = 1;
    private PendingIntent mGeofenceIntent = null, mNotificationIntent = null;

    private static Util util;

    private Context mContext;


    private Util(Context context) {
        mContext = context;

        mGeofencingClient = LocationServices.getGeofencingClient(context);
        mNotificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public static Util getInstance(Context context) {
        if (util == null) {
            util = new Util(context);
        }
        return util;
    }


    @SuppressLint("MissingPermission")
    public void addReminderGeoFence(int reminderId, Double lat, Double lng, int radius) {

        mReminderId = reminderId;
        ArrayList<Geofence> mListGeofences = new ArrayList<>();
        mListGeofences.add(
                new Geofence.Builder()
                        .setRequestId("" + reminderId)
                        .setCircularRegion(lat, lng, radius)
                        .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER)
                        .setExpirationDuration(-1)
                        .build()
        );

        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);
        builder.addGeofences(mListGeofences);
        GeofencingRequest geofencingRequest = builder.build();


        mGeofencingClient.addGeofences(
                geofencingRequest,
                getPendingIntent(mReminderId)
        ).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(mContext, "Geofence Added", Toast.LENGTH_SHORT).show();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(mContext, "Geofence Not Added", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void RemoveGeofence() {
        mGeofencingClient.removeGeofences(getPendingIntent(mReminderId))
                .addOnSuccessListener((Activity) mContext, new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.e("RemoveGeofence", "Success: " + mReminderId);
                    }
                })
                .addOnFailureListener((Activity) mContext, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.e("RemoveGeofence", "Fail: ");
                    }
                });

    }

    private PendingIntent getPendingIntent(int reminderId) {
        if (mGeofenceIntent == null) {
            Intent intentReminderBroadcast = new Intent(ACTION_ENTER);
            intentReminderBroadcast.putExtra(KEY_EXTRA_REMINDER_ID, reminderId);

            mGeofenceIntent =
                    PendingIntent.getBroadcast(
                            mContext,
                            PENDING_INTENT_ID,
                            intentReminderBroadcast,
                            PendingIntent.FLAG_UPDATE_CURRENT

                    );
        } else {

        }
        return mGeofenceIntent;
    }


    public void sendNotification(String GeofenceId) {

        int reminderId = Integer.parseInt(GeofenceId);
        Log.e("GeofenceDetails", " id-------------- " + reminderId);


        DBUtil.getInstance(mContext);

        String[] selecitonArgs = new String[]{String.valueOf(reminderId)};
        Cursor c = DBUtil.mDbReminders.query("reminders", null, "reminder_id = ?", selecitonArgs, null, null, null);

        while (c.moveToNext()) {
            String Title = c.getString(1);
            String Description = c.getString(2);
            LatLng location = new LatLng(Double.parseDouble(c.getString(3)),
                    Double.parseDouble(c.getString(4)));
            int Radius = c.getInt(5);
            int Repeat = c.getInt(6);
            int Status = c.getInt(7);
            String Date = c.getString(8);


            Log.e("tag", "sendNotification:------ " + reminderId + " " + Title + "----" + Description + "------" + Status);


            Reminder reminder = new Reminder(reminderId, Radius, Title, Description, location, Repeat, Status, Date);
            getNotificationIntent(reminder);

            Log.e("tag", reminder.getReminderId() + "");

            if (Status == 1) {
                if (Repeat == 0) {
                    DBUtil.getInstance(mContext).updateReminderStatus(reminderId, 0);
                }

                NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext);
                notificationBuilder
                        .setSmallIcon(R.drawable.ic_reminder)
                        .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_reminder))
                        .setColor(Color.RED)
                        .setContentTitle(Title)
                        .setContentText(Description)
                        .setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND)
                        .setContentIntent(mNotificationIntent)
                        .setAutoCancel(true)

                ;
                mNotificationManager.notify(reminderId, notificationBuilder.build());
            } else {

            }

        }

        c.close();

    }

    public void getNotificationIntent(Reminder reminder) {

        Intent intent = new Intent(mContext, MainActivity.class);
        intent.putExtra(KEY_EXTRA_REMINDER_ID, reminder.getReminderId());

        mNotificationIntent =
                PendingIntent.getActivity(
                        mContext,
                        PENDING_INTENT_ID,
                        intent,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );

    }
}





